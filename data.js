export default{
	products: [
	  {  
	    _id:'1',
		name: 'Apple MacBook Air [silver]',
		image:'public/images/Mac1.jpg',
		category: 'Mac Laptops',
		size:'13 inch',
		price:'Rs. 100000',
		countInStock:0
		
	  },
	  {
		_id:'2',
		name: 'Apple MacBook Pro [space grey]',
		image:'public/images/Pro1.jpg',
		category: 'Mac Laptops',
		size:'13 inch',
		price:'Rs. 160000',
        countInStock:3	
		
	  },
	  { 
	    _id:'3',
		name: ' Apple IPhone XR ',
		image:'public/images/IPhone1.jpg',
		category: 'IPhones',
		size:'6.1 inch',
		price:'Rs. 47000',
        countInStock:3
		
	  },
	  {  
	    _id:'4',
		name: 'Apple IPhone 11',
		image:'public/images/IPhone2.jpg',
		category: 'IPhones',
		size:'6.1 inch',
		price:'Rs. 63999',
        countInStock:3
		
	  },
	  {
		_id:'5',
		name: 'Apple IPhone 11',
		image:'public/images/IPhone2.jpg',
		category: 'IPhones',
		size:'6.1 inch',
		price:'Rs. 63999',
        countInStock:3
		
	  },
	  { 
	    _id:'6',
		name: 'Apple IPhone Pro Max',
		image:'public/images/IPhone3.jpg',
		category: 'IPhones',
		size:'6.5 inch',
		price:'Rs. 109099',
        countInStock:3		
	  },
	  {
		_id:'7',
		name: 'Apple Watch Series 5 [Space Grey]',
		image:'public/images/AppleWatch2.jpg',
		category: 'Apple Watches',
		size:'44mm',
		price:'Rs. 40900',
        countInStock:3
		
	  },
	  { 
	    _id:'8',
		name: 'Apple Watch Series 5 [Silver Aluminium]',
		image:'public/images/AppleWatch1.jpg',
		category: 'Apple Watches',
		size:'44mm',
		price:'Rs. 43900',
        countInStock:3		
	  },
	
	
	]	
}