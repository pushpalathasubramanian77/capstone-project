import React, { Component } from 'react'  
import axios from 'axios';  
import { Pie } from 'react-chartjs-2';  

export class ChartScreen extends Component {  

        constructor(props) {  
                super(props);  

                this.state = { Data: {} };  

        }  

        componentDidMount() {  

                axios.get("http://localhost:5000/api/chart")  

                        .then(res => {  

                                console.log(res);  

                                const ProductList = res.data;  

                                let productName = [];  

                                let countInStock = [];  

                                ProductList.forEach(record => {  
                                        
										productName.push(record.name);  

                                        countInStock.push(record.countInStock);  

                                });  

                                this.setState({  

                                        Data: {  

                                                labels: productName,  

                                                datasets: [  

                                                        {  

                                                                label: 'Products and Its Available Stock',  

                                                                data: countInStock,  

                                                                backgroundColor: [  

                                                                        "#3cb371",  

                                                                        "#0000FF",  

                                                                        "#9966FF",  

                                                                        "#4C4CFF",  

                                                                        "#00FFFF",  

                                                                        "#f990a7",  

                                                                        "#aad2ed",  

                                                                        "#FF00FF",
																		
																		"Blue",
																		
																		"Red"

                                                                ]  

                                                        }  

                                                ]  
                                        }  

                                });  

                        })  

        }  

        render() {  

                return (  

                        <div>  

                                <Pie data={this.state.Data}  

                                        options={{ maintainAspectRatio: false }} ></Pie>  

                        </div>  

                )  
        }  
}  

export default ChartScreen;