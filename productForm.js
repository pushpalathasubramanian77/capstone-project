import React from 'react';
import {withFormik, Form, Field} from 'formik';
import * as Yup from 'yup';

const ProductForm =({ values, errors, touched, isSubmitting }) => (

  <div> 
    <h1> Add New Product</h1> 
    <Form> 
         <div>  
             <Field type="text" name="name" placeholder="name"/> 
			 {touched.name&&errors.name&&<span style={{ color: 'red' }}>{errors.name}</span>} 
			 <br/>
			 <br/>
			 
			 <Field type="text" name="image" placeholder="image"/> 
			 {touched.image&&errors.image&&<span style={{ color: 'red' }}>{errors.image}</span>} 
			 <br/>
			 <br/>
			 
			 <Field as = "select" name="category"  placeholder="category">
			 
			      <option value="Mac Laptops" label="Mac Laptops" />
                  <option value="IPhones" label="IPhones" />
                  <option value="Apple Watches" label="Apple Watches" /> 
			
			</Field>

            {touched.category&&errors.category&&<span style={{ color: 'red' }}>{errors.category}</span>} 
			 <br/>
			 <br/>
			 
			 <Field type="text" name="size" placeholder="size"/> 
			 {touched.size&&errors.size&&<span style={{ color: 'red' }}>{errors.size}</span>} 
			 <br/>
			 <br/>
			 
			 <Field type="text" name="price" placeholder="price"/> 
			 {touched.price&&errors.price&&<span style={{ color: 'red' }}>{errors.price}</span>} 
			 <br/>
			 <br/>
			 
			 <Field type="text" name="countInStock" placeholder="countInStock"/> 
			 {touched.countInStock&&errors.countInStock&&<span style={{ color: 'red' }}>{errors.countInStock}</span>} 
			 
			 
			 
         </div> 
		 
          <br/> 
          <button onClick={() => {this.handleSubmit()}} type="submit" > >Submit</button> 

    </Form> 
   </div>	
)
const FormikProductForm = 
             withFormik({ 
                          mapPropsToValues({name,image,category,size,price,countInStock})
						  { 
                             return { 
                                    name: name ||'',
                                    image: image ||'',									
									category: category || '',
									size: size ||'',
									price: price || '',
									countInStock: countInStock ||''
                                     
                                  } 
                         }, 

                         validationSchema: Yup.object().shape({ 
                               name: Yup.string().required('Product Name is required'),
							   image: Yup.string().required('local path of the image is required - eg.. public/images/xxxx.jpg'),
                               category: Yup.string().required('category is required'),
                               size: Yup.string().required('size is required'),
							   price: Yup.string().required('Price is required'),
							   countInStock: Yup.string().required('countInStock is required')
                            }), 

                          handleSubmit(values,{ resetForm, setSubmitting, setErrors }) { 
                            console.log(values); 
                            setTimeout(() => { 
							                   alert("Added successfully") ;
                                               this.props.onAddProduct(values);
                                               
                                              

                                            },2000); 

                         },
                         						 

                        })(ProductForm) 
						

 

export default FormikProductForm





























/*export default class ProductForm extends React.Component{
	constructor(props){
		super(props);
		
		this.onSubmit = this.onSubmit.bind(this);
	}
	
	onSubmit(event){
	  event.preventDefault();
      var product = {};
	  product.productName = this.refs.productName.value;
	  product.quantity = this.refs.quantity.value;
	  product.price = this.refs.price.value;
	  this.props.onSave(product);
      	  
	}
	

   render(){
	   return(
	      <>
	      <form>
		    <h1> Add Product </h1>
            ProductName : &nbsp; 
			<input type="text" 
			   ref="productName"  
                placeholder="productName" onChange={this.handleChange.bind(this, "productName")} value={this.state.fields["productName"]}/>

			   <br/> <br/>
			Quantity : &nbsp; 
			<input type="number" 
			   ref="quantity" /> <br/> <br/>
			Price : &nbsp; 
			<input type="text" 
			   ref="price" /> <br/> <br/>
			<input type="submit" value="Save" onClick={this.onSubmit} />
		  </form>
		  </>
	   );
   }	   	
}*/