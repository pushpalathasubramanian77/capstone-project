import React ,{ Component } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import { saveProduct } from '../actions/addProductAction';
import ProductForm from '../actions/productForm';
import {bindActionCreators} from 'redux';



export class AddProductScreen extends Component {


    constructor(props) {
        super(props)

       this.addProduct=this.addProduct.bind(this);
                      }
    
    addProduct(product){
        console.log("product" +product);
        this.props.actions.addProduct(product);
		
    }
    
    render() {
		
		return(
		         <div>
                  <ProductForm onAddProduct={this.addProduct}/>
				  
                 </div> 
				  
		
		);
		
		
	}
	
}


function mapStateToProps(state){
    return {
      product: state.product
    };
}

 
function mapDispatchToProps(dispatch){
     return{
            actions: bindActionCreators(saveProduct,dispatch)
      }

}

export default connect(mapStateToProps,mapDispatchToProps)(AddProductScreen);















/*function AddProductScreen (props){
	
	const newProductDetails = useSelector(state => state.newProductDetails);
	const dispatch = useDispatch();
	
	
	useEffect( () => {
		
		dispatch(saveProduct(newProductDetails));
	    
		return () =>  {
			
		};
	
	},[])
	
	
	 return(
	      <div>
	      <ProductForm onSave= {(newProductDetails) => this.saveProduct(newProductDetails)} />
		  </div>
	   );
	
	
}


export default AddProductScreen; */